/*
    sonde_cc.c

    (C) 2021 Frédéric Mantegazza

    Licenced under GPL:

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:

    - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    - Neither the name of the nor the names of its contributors may be used to endorse or
    promote products derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
    IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
    FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Includes
#include <stdint.h>
#include <stddef.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>


// Global vars
static volatile uint8_t g_button;    // button state
static volatile uint8_t g_toneFlag;  //
static volatile uint8_t g_reminderCounter;


static void initTimers(void)
{
    // Timer 0 (frequency generator)
    TCCR0A = _BV(COM0A0)  // Toggle OC0A on Compare Match
           | _BV(WGM01);  // CTC mode

    TCCR0B = _BV(CS01);  // CLKio/8

    // Timer 1 (reminder generator)
    TCCR1 = _BV(CS13)
          | _BV(CS12)
          | _BV(CS11)
          | _BV(CS10);  // CLKio/16384

    TIMSK = _BV(TOIE1);  // Timer/Counter1 Overflow Interrupt Enable
}


static void initIO(void)
{

    // Button pin as input (with pullup enable)
    DDRB  = _BV(PB3);
    PORTB = _BV(PB3);

    // Interrupts
    GIMSK = _BV(PCIE);    // Pin Change Interrupt Enable
    PCMSK = _BV(PCINT3);  // enable PCINT3

    // Unused pins as inputs...
    DDRB |= _BV(PB2)
         |  _BV(PB1)
         |  _BV(PB0);

    // ...with pullups
    PORTB |= _BV(PB2)
          |  _BV(PB1)
          |  _BV(PB0);
}


static void initAdc(void)
{

    // ADC Prescalar set to 8 - 125kHz@8MHz
    ADCSRA = _BV(ADPS1)
           | _BV(ADPS0);

    // Vcc as Ref
    // ADC left adjusted
    // ADC2 as input
    ADMUX = _BV(ADLAR)
          | _BV(MUX1);

    // Free Running Mode
//     ADCSRB = 0x00;

    // Disable digital input on ADC2
    DIDR0 = _BV(ADC2D);

    // ADC enable
    // Start conversion
    // Auto-trigger mode
     ADCSRA |= _BV(ADEN)
            |  _BV(ADSC)
            |  _BV(ADATE);
}


// Timer 1 overflow interrupt routine
ISR(TIMER1_OVF_vect)
{
    if (!g_toneFlag) {
        g_reminderCounter++;
    }
}


// IO interrupt routine
ISR(PCINT0_vect)
{
    // Detect a falling edge on button
    if (!(PINB & _BV(PB3))) {
        g_button = 1;
    }
}


// Core
int main(void)
{
    static uint8_t adcValue;
    static uint8_t adcReferenceValue;

    // Init hardware
    initTimers();
    initIO();
    initAdc();
    _delay_ms(100);

    // Init global vars
    g_button = 0;
    g_toneFlag = 1;
    g_reminderCounter = 0;

    // Initial ADC reference value (-> mid frequency)
    adcReferenceValue = ADCH;

    // 'Stepify' and divide by 2
//     adcReferenceValue >>= 3;
//     adcReferenceValue <<= 1;

    sei();
    while (1) {

        // Read ADC value
        adcValue = ADCH;

        // Turn off timer0 if probes are open
        if (g_toneFlag && (adcValue >= 240)) {
            TCCR0A &= !_BV(COM0A0);  // OC0A disconnected
            g_toneFlag = 0;
        }

        // Turn on timer0 if probes are closed
        else if (!g_toneFlag && (adcValue < 240)) {
            TCCR0A |= _BV(COM0A0)
                   |  _BV(WGM01);
            g_toneFlag = 1;
        }

        // 'Stepify' and divide by 2
//         adcValue >>= 3;
//         adcValue <<= 1;

        // Test button state
        if (g_button) {

            // Set current ADC value as reference value (-> mid frequency)
            adcReferenceValue = adcValue - 8;

            // Reset button state
            g_button = 0;

            // Debounce
            _delay_ms(20);
        }

        // Set frequency
//         int8_t value = adcValue - adcReferenceValue;
//         OCR0A = 127 + value;
        OCR0A = adcValue;

        // Reminder bip (every ~30s)
        if (g_reminderCounter == 7) {
            if (!g_toneFlag) {
                TCCR0A |= _BV(COM0A0) | _BV(WGM01);
                OCR0A = 32;  // ~2kHz
                _delay_ms(100);
                TCCR0A &= !_BV(COM0A0);
            }
            g_reminderCounter = 0;
        }

        _delay_ms(50);
    }

    return 1;
}
